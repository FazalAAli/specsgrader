from flask_sqlalchemy import SQLAlchemy
from datetime import datetime, timedelta
import requests
from config import CanvasAPIConfig
import html
db = SQLAlchemy()


class Association(db.Model):
    __tablename__ = "association"
    course_id = db.Column(db.ForeignKey('courses.id'), primary_key=True)
    user_id = db.Column(db.ForeignKey('users.id'), primary_key=True)
    grade = db.Column(db.Text)

    def __init__(self, course_id, user_id):
        self.course_id = course_id
        self.user_id = user_id


class Course(db.Model):
    __tablename__ = "courses"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    default_virt_token = db.Column(db.Integer)
    grade_rubric = db.Column(db.Text, nullable=False)
    vt_redemption = db.Column(db.Boolean, default=True)

    def __init__(self, course_information):
        self.update_settings(course_information)

    def update_settings(self, course_information):
        self.id = int(course_information["course_id"])
        self.name = html.escape(course_information["name"])
        self.default_virt_token = int(course_information["vtNo"])
        self.grade_rubric = str.lower(
            html.escape(course_information["rubric"]))
        self.vt_redemption = course_information["vtRedemption"]


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    access_token = db.Column(db.Text)
    name = db.Column(db.Text)
    refresh_token = db.Column(db.Text)
    expiry = db.Column(db.DateTime)

    def __init__(self, user_information):
        self.id = int(user_information["user"]["id"])
        self.access_token = user_information["access_token"]
        self.name = user_information["user"]["name"]
        self.refresh_token = user_information["refresh_token"]
        self.expiry = datetime.utcnow() + timedelta(seconds=3600)

    def update(self, user_information):
        self.access_token = user_information["access_token"]
        self.refresh_token = user_information["refresh_token"]
        self.expiry = datetime.utcnow() + timedelta(seconds=3600)
        self.name = user_information["user"]["name"]
        db.session.commit()

    def get_access_token_safely(self):
        access_token = None

        time_now = datetime.utcnow()
        if(time_now > self.expiry):
            # Need to get new accesss token with refresh because old has expired
            url = CanvasAPIConfig.CANVAS_URL + "/login/oauth2/token"

            parameters = {
                "grant_type": "refresh_token",
                "client_id": CanvasAPIConfig.CANVAS_CLIENT_ID,
                "client_secret": CanvasAPIConfig.CANVAS_SECRET,
                "refresh_token": self.refresh_token
            }

            access_token_request = requests.post(url, params=parameters)
            user_details = access_token_request.json()

            # user_details = do_request("/login/oauth2/token", self, parameters)

            self.access_token = user_details["access_token"]
            self.expiry = datetime.utcnow() + timedelta(seconds=3600)
            db.session.commit()

        access_token = self.access_token

        return access_token
