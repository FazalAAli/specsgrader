import requests
from main import redis_session
from models import User
from config import CanvasAPIConfig


def do_request(url, user, params={}, headers={}, json={}, method="GET"):

    full_url = CanvasAPIConfig.CANVAS_URL + url
    params["access_token"] = user.get_access_token_safely()

    if (len(json) > 0):
        headers["Content-Type"] = "application/json"

    response = requests.request(method, full_url, params=params,
                                headers=headers, json=json)

    jsonned = None

    jsonned = response.json()

    return jsonned


def get_user_from_uid(uid):
    if(len(uid) == 0):
        return None
    id = redis_session.get(uid).decode('utf-8')
    select_user = User.query.get(id)
    return select_user
