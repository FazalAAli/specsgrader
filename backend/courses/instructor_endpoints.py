from utility import *
from flask import jsonify, request, make_response
from main import app
from models import *
from .course_utilities import *


@ app.route("/api/courses/instructor", methods=["POST"])
def get_single_course_detail_instructor():
    uid = request.cookies.get("uid")
    course_id = int(request.json["course_id"])
    user = get_user_from_uid(uid)
    course = Course.query.get(course_id)

    if not course:
        return jsonify({"grade": "Course not set up", "studentDetails": []})

    studentDetails = []

    students, all_grades, grade = get_course_average_grade(
        course, user, update=True, giveAll=True)

    for index, student in enumerate(students):
        studentDetails.append(
            {"name": student["name"], "grade": all_grades[index], "id": student["id"]})

    return jsonify({
        "grade":  grade,
        "studentDetails": studentDetails
    })


@ app.route("/api/courses/instructor/submit_grades", methods=["POST"])
def submit_grades():
    uid = request.cookies.get("uid", default="")
    user = get_user_from_uid(uid)
    params = request.json
    course = Course.query.get(int(params["course_id"]))

    if not course:
        return make_response({"message": "Course not set up"}, 400)

    students = do_request(f"/api/v1/courses/{course.id}/users",
                          user, {"enrollment_type": "student"})

    all_grades, grade_count = get_students_grades(
        course, user, students, True)

    specs_grader_entry_id = get_specs_grader_entry(course.id, user)[0]["id"]

    for index, student in enumerate(students):
        grade = all_grades[index]
        student_id = student["id"]
        body = {
            "submission": {
                "posted_grade": grade
            },
            "comment": {
                "text_comment": "Graded by Specs Grader"
            }
        }

        do_request(
            f"/api/v1/courses/{course.id}/assignments/{specs_grader_entry_id}/submissions/{student_id}",
            user, json=body, method="PUT")

    return make_response({"message": "Done"}, 200)


@ app.route("/api/courses/instructor/init_course", methods=["POST"])
def init_course():
    uid = request.cookies.get("uid", default="")
    user = get_user_from_uid(uid)
    params = request.json

    if(len(params["vtNo"]) < 1 or len(params["rubric"]) < 1):
        response = make_response(
            {"message": "vtNo and/or rubric cannot be empty"})
        return response, 400

        # print(info_provided)
        # Check if user is teacher in course

    course_valid = False
    if(user):
        courses = get_users_courses(user)
        for course in courses:
            if course["id"] == params["course_id"]:
                course_valid = course["enrollment"] == "teacher"
                break

    if not course_valid:
        response = make_response(
            {"message": "You are not authenticated in this course as teacher; Please log out and log in again to refresh cred"})
        return response, 401

    selected_course = Course.query.get(int(params["course_id"]))
    if(selected_course):
        selected_course.update_settings(params)
    else:
        selected_course = Course(params)
        add_specs_grader_entry(selected_course.id, user)
        db.session.add(selected_course)

    db.session.commit()

    response = make_response({"message": "Course settings saved"})
    return response, 200
