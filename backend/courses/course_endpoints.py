from utility import *
from flask import jsonify, request, make_response
from main import app
from models import *
from .course_utilities import get_users_courses

from courses.instructor_endpoints import *  # nopep8
from courses.student_endpoints import *  # nopep8


@ app.route("/api/courses", methods=["POST"])
def get_my_courses():
    uid = request.cookies.get("uid")
    update = request.json["update"]
    user = get_user_from_uid(uid)
    courses = []
    if(user):
        courses = get_users_courses(user, update)

    response = make_response(jsonify(courses))

    return response
