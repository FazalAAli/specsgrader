from config import OtherConfig
from utility import *
from models import *
import pandas as pd
from io import StringIO


def get_specs_grader_entry(course_id: int, user: User):
    possible_sg_entry = do_request(
        f"/api/v1/courses/{course_id}/assignments", user, params={"search_term": "SpecsGrader"})

    return possible_sg_entry

# Add the SpecsGrader entry to Canvas so it can be exported at the end of course.


def add_specs_grader_entry(course_id: int, user: User):
    possible_sg_entry = get_specs_grader_entry(course_id, user)

    if(len(possible_sg_entry) > 0):
        return possible_sg_entry

    body = {
        "assignment": {
            "name": "SpecsGrader",
            "submission_types": ["none"],
            "grading_type": "letter_grade",
            "published": True
        }
    }

    response = do_request(
        f"/api/v1/courses/{course_id}/assignments", user, json=body, method="POST")

    return response


# For all students in a course, get their all of their individual grades


def get_students_grades(course: Course, authenUser: User, students, update=False):

    grades = []
    grade_count = {}
    for key in OtherConfig.GRADE_MAP.keys():
        grade_count[key] = 0

    rubric_conversion = pd.read_csv(StringIO(course.grade_rubric))
    for student in students:
        student_grade = get_users_grade(
            course.id, student["id"], authenUser, rubric_conversion, update)

        grade_count[student_grade] += 1

        grades.append(student_grade)

    return(grades, grade_count)


# Given a bunch of grade counts (how many As, Bs, etc. there are),
# calculate the average


def count_average(grade_count, numStudents):

    average_grade = 0
    grade = "ERR"
    for key, value in grade_count.items():
        average_grade += OtherConfig.GRADE_MAP[key] * value

    average_grade = average_grade/numStudents

    for letter, mapped_grade in OtherConfig.GRADE_MAP.items():
        if (mapped_grade > average_grade):
            continue
        else:
            grade = letter
            break

    return grade


# For a course, get the average grade of all students. Also it can return all the grades used for the average
# so there's one less call to the API


def get_course_average_grade(course: Course, user: User, update=False, giveAll=False):

    if giveAll:
        update = True

    course_assoc = db.session.query(Association).filter_by(
        course_id=course.id, user_id=user.id).first()

    if not course_assoc:
        course_assoc = Association(course.id, user.id)
        db.session.add(course_assoc)

    if course_assoc.grade and not update:
        print("Returning old value of course grade")
        return course_assoc.grade

    students = do_request(f"/api/v1/courses/{course.id}/users",
                          user, {"enrollment_type": "student"})

    print("Calculating course grade")
    grade = "ERR"

    all_grades, grade_count = get_students_grades(
        course, user, students, update)

    grade = count_average(grade_count, len(students))

    course_assoc.grade = grade
    db.session.commit()

    if(giveAll):
        return students, all_grades, grade
    else:
        return grade


def get_users_courses(user: User, update=False):
    course_info = do_request("/api/v1/courses", user)
    to_return = []

    if not course_info:
        return to_return

    for course in course_info:
        enrollment = course["enrollments"]
        enrollment_type = enrollment[0]["type"]
        course_id = int(course["id"])
        db_entry = Course.query.get(course_id)
        initialized = True if db_entry else False

        if(not initialized and enrollment_type == "student"):
            continue

        course_info_to_return = {
            "course_code": course["course_code"],
            "name": course["name"],
            "enrollment": enrollment_type,
            "id": course_id,
            "initialized": initialized
        }

        if enrollment_type == "teacher":
            # CHECK IF COURSE IS IN DATABASE AND RETURN GRADE IF IT IS
            if(initialized):
                course_info_to_return["grade"] = "AVG: " + get_course_average_grade(
                    db_entry, user, update)
            else:
                course_info_to_return["grade"] = "Init course"
        else:
            # CALC STUDENT GRADE AND VT
            if(initialized):
                course_info_to_return["grade"] = get_users_grade(
                    course_id, user.id, user, pd.read_csv(StringIO(db_entry.grade_rubric)), update)
            else:
                course_info_to_return["grade"] = "Course Not Set up"

        to_return.append(course_info_to_return)

    return to_return


# For a user in a course, get their assignments and calculate their grade


def calc_grade(course_id, user_id, authenUser, assignments, rubric: pd.DataFrame):

    types_of_assignments = rubric.columns.tolist()
    count = {}

    # init
    for assType in types_of_assignments:
        if(assType != "grade"):
            category = str.lower(assType).strip()
            count[category] = 0

    for assignment in assignments:
        type_ass = str.lower(assignment["name"])
        assignment_id = assignment["id"]
        assignment_details = do_request(f"/api//v1/courses/{course_id}/assignments/{assignment_id}/submissions/{user_id}", authenUser)  # nopep8
        if(assignment_details):
            if(assignment_details["score"]):
                assignment_score = int(assignment_details["score"])
                for key in count.keys():
                    if(key in type_ass):
                        if(assignment_score >= 2):
                            count[key] += 1

    for index, row in rubric.iterrows():
        all_greater_or_equal = True
        for key in count.keys():
            if(count[key] < int(row[key])):
                # We didn't meet criteria here
                all_greater_or_equal = False
                break

        if(all_greater_or_equal):
            return str.upper(row["grade"])
    return "F"


def get_users_grade(course_id, user_id, authenUser: User, course_rubric: pd.DataFrame, update=False):
    course_assoc = db.session.query(Association).filter_by(
        course_id=course_id, user_id=user_id).first()

    if not course_assoc:
        course_assoc = Association(course_id, user_id)
        db.session.add(course_assoc)

    if course_assoc.grade and not update:
        print("Returning old value of user grade")
        return course_assoc.grade

    print("Calculating user grade")

    assignments = do_request(
        f"/api/v1/users/{user_id}/courses/{course_id}/assignments", authenUser)

    grade = calc_grade(course_id, user_id, authenUser,
                       assignments, course_rubric)

    course_assoc.grade = grade
    db.session.commit()

    # response = requests.get(url, params=params)
    # course_info = response.json()
    # print(response)
    return grade
