from utility import *
from flask import jsonify, request
from main import app
from models import *


@app.route("/api/courses/student/assignments", methods=["POST"])
def get_students_assignments():
    assignment_information = []
    uid = request.cookies.get("uid")
    user = get_user_from_uid(uid)
    course_id = int(request.json["course_id"])
    student_id = int(request.json["student_id"])

    if(student_id == -1):
        student_id = user.id

    assignments = do_request(
        f"/api/v1/users/{student_id}/courses/{course_id}/assignments", user)

    for assignment in assignments:
        assignment_id = assignment["id"]
        assignment_details = do_request(f"/api//v1/courses/{course_id}/assignments/{assignment_id}/submissions/{student_id}", user)  # nopep8
        if(assignment_details):
            assignment_information.append({
                "id": assignment_id,
                "name": assignment["name"],
                "score": assignment_details["score"]
            })

    return jsonify(assignment_information)
