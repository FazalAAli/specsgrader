from flask import Flask, jsonify, request
from config import ApplicationConfig
from flask_redis import FlaskRedis
from flask_cors import CORS, cross_origin
from models import *

app = Flask(__name__)
app.config.from_object(ApplicationConfig)
db.init_app(app)
redis_session = FlaskRedis(app)
CORS(app, supports_credentials=True)

# Import endpoint definitions from everywhere
from courses.course_endpoints import *  # nopep8
from login.login_endpoints import *  # nopep8


if __name__ == "__main__":
    with app.app_context():
        db.create_all()
    app.run(host="localhost", port=5000)


@app.route("/api/")
@cross_origin(supports_credentials=True)
def api_home():
    s = "You have reached the api"

    uid = request.cookies.get("uid")
    if uid:
        user = get_user_from_uid(uid)
        user.get_access_token_safely()
        if user:
            s += f" and you are logged in as {user.name}"

    return jsonify({
        "message": s
    })
