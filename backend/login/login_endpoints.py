from flask import jsonify, request, make_response
from main import app, redis_session
from models import *
from uuid import uuid1
from login.login_utilities import *


@app.route("/api/login", methods=["POST"])
def login():
    # Convert code to access token
    canvas_code = request.json["canvas_code"]
    url = CanvasAPIConfig.CANVAS_URL + "/login/oauth2/token"
    parameters = {
        "grant_type": "authorization_code",
        "client_id": CanvasAPIConfig.CANVAS_CLIENT_ID,
        "client_secret": CanvasAPIConfig.CANVAS_SECRET,
        "redirect_uri": CanvasAPIConfig.FRONTEND_REDIRECT_URI,
        "code": canvas_code
    }

    access_token_request = requests.post(url, params=parameters)
    user_details = access_token_request.json()

    # Check if canvas has the user's information
    grant = user_details["user"]["id"]
    response, code = None, None
    if grant:
        # User exists, add to database and store their session information
        check_and_add(user_details)
        uid = uuid1()
        redis_session.set(str(uid), int(user_details["user"]["id"]))
        response = make_response({"uid": uid})
        code = 200
    else:
        response = make_response(
            {"error": "OAuth flow did not return user information"})
        code = 401
    return response, code


@app.route("/api/logout")
def api_logout():
    uid = request.cookies.get("uid")

    if uid:
        s = "You have been logged out"
        redis_session.delete(uid)
    else:
        s = "Nothing to log out"

    return jsonify({
        "message": s
    })
