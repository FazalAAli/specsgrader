from config import CanvasAPIConfig
from models import *
import requests


def check_and_add(user_information):
    id = int(user_information["user"]["id"])
    selected_user = User.query.get(id)

    if selected_user:
        # User already in database
        # Revoke old information
        access_token = selected_user.get_access_token_safely()
        if(len(access_token) > 0):
            # Revoke old token so canvas doesnt show too many tokens
            # Cannot delete without use so make a simple call in case token never used before
            url = CanvasAPIConfig.CANVAS_URL + "/api/v1/users/self/profile"
            parameters = {
                "access_token": access_token
            }
            requests.get(url, params=parameters)

            # Revoke call
            url = CanvasAPIConfig.CANVAS_URL + "/login/oauth2/token"
            requests.delete(url, params=parameters)

        # Update new inforamtion
        selected_user.update(user_information)
    else:
        # New user - put in database
        new_user = User(user_information)
        db.session.add(new_user)

    db.session.commit()
