import os
import redis
from dotenv import load_dotenv

load_dotenv()


class ApplicationConfig():
    REDIS_URL = "redis://127.0.0.1:6379"
    SECRET_KEY = os.environ.get("SECRET_KEY")
    DEBUG = os.environ.get("DEBUG")
    SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get(
        "SQLALCHEMY_TRACK_MODIFICATIONS")


class CanvasAPIConfig():
    CANVAS_URL = os.environ.get("CANVAS_URL")
    CANVAS_SECRET = os.environ.get("CANVAS_SECRET")
    CANVAS_CLIENT_ID = os.environ.get("CANVAS_CLIENT_ID")
    FRONTEND_REDIRECT_URI = os.environ.get("REDIRECT_URI")


class OtherConfig():
    FRONTEND_URL = os.environ.get("FRONTEND_URL")
    GRADE_MAP = {
        "A": 94,
        "A-": 90,
        "B+": 87,
        "B": 83,
        "B-": 80,
        "C+": 77,
        "C": 73,
        "C-": 70,
        "D+": 67,
        "D": 63,
        "D-": 60,
        "F": 50
    }
