import os
import sys
topdir = os.path.join(os.path.dirname(__file__), "..")
sys.path.append(topdir)

from main import app  # , db  # nopep8
from models import *  # nopep8

# database = db
client = app.test_client()
