from unittest import main, TestCase
import json
from test_client import client


class backend_test(TestCase):
    def test_1(self):
        reponse = client.get("/api/")
        self.assertEqual(reponse.status_code, 200)


if __name__ == "__main__":  # pragma: no cover
    main()
