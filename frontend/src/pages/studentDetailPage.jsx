import React from "react"
import StudentGradeTable from "../components/studentGradeTable";

export default function StudentDetailsPage(props) {
    return <>
        <div className="col-lg-6 col-sm-12 col-md-12">

            <h5 className="pt-2">Your grade is {props.existingInformation.grade}</h5>
            <StudentGradeTable student_id="-1" course_id={props.existingInformation.id} />
        </div>
    </>;
}