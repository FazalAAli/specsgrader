import { Accordion, Form, Button, InputGroup, FormControl, Alert, Table } from "react-bootstrap";
import { React, useState } from "react";
import { useEffect } from "react";
import { getSingleCourseDataInstructor } from "../components/utility"
import StudentDetailModal from "../components/studentDetailModal"

function CourseSettings(props) {
    const [message, setMessage] = useState("")
    const [variant, setVariant] = useState("")
    let existingInformation = props.existingInformation;

    async function submitForm(e, existingInformation) {
        e.preventDefault();
        setMessage("Loading...")
        setVariant("info")
        var endpoint = process.env.REACT_APP_FLASK_API + "/courses/instructor/init_course"

        var send_info = await fetch(endpoint, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "course_id": existingInformation.id,
                "name": existingInformation.name,
                "vtNo": e.target[0].value,
                "rubric": e.target[1].value,
                "vtRedemption": e.target[2].checked
            })
        });

        let processed = await send_info.json();
        if (send_info.status !== 200) {
            setMessage(processed["message"])
            setVariant("danger")
            //alert("Could not complete: " + processed["message"])
        } else {
            setMessage(processed["message"])
            setVariant("success")
            //alert("Request successful " + processed["message"])
        }

        return
    }

    async function submitGrades(existingInformation) {
        setMessage("Loading...")
        setVariant("info")
        var endpoint = process.env.REACT_APP_FLASK_API + "/courses/instructor/submit_grades"

        var send_info = await fetch(endpoint, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "course_id": existingInformation.id,
            })
        });

        let processed = await send_info.json();
        if (send_info.status !== 200) {
            setMessage(processed["message"])
            setVariant("danger")
        } else {
            setMessage(processed["message"])
            setVariant("success")
        }

        return
    }

    const showSettings = existingInformation ? !existingInformation.initialized : false;

    return (
        <>
            <Accordion className="pb-2" defaultActiveKey={showSettings ? "0" : "1"}>
                <Accordion.Item eventKey="0">
                    <Accordion.Header>
                        <p>Settings</p>
                    </Accordion.Header>
                    <Accordion.Body>

                        <Alert dismissible className={message ? "d-block" : "d-none"} id="status" variant={variant} onClose={() => setMessage("")}>
                            {message}
                        </Alert>

                        <Form onSubmit={(e) => submitForm(e, existingInformation)}>

                            <Form.Group className="mb-3" controlId="vtNo">
                                <Form.Label>Default Virtual Tokens(VT)</Form.Label>
                                <Form.Control type="text" placeholder="15" />
                            </Form.Group>

                            <InputGroup>
                                <InputGroup.Text>Grading Rubric (csv)</InputGroup.Text>
                                <FormControl as="textarea" aria-label="rubric" />
                            </InputGroup>

                            <Form.Text className="text-muted">
                                Find an example <a href="#">here</a>
                            </Form.Text>

                            <Form.Group className="mb-3" controlId="vtRedemption">
                                <Form.Check type="checkbox" label="Automatic VT Redemption?" defaultChecked />
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>

                        <hr />

                        <Button onClick={() => { submitGrades(existingInformation) }} variant="primary">
                            Submit Grades to Canvas
                        </Button>
                        <p className="text-muted">This takes a while!</p>
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
        </>
    );
}

export default function InstructorDetailsPage(props) {

    let existingInformation = props.existingInformation
    const [message, setMessage] = useState([])
    const [loaded, setLoaded] = useState(false)
    const [studentDetails, setStudentDetails] = useState([])

    useEffect(() => {
        setLoaded(false)
        getSingleCourseDataInstructor(existingInformation.id, setMessage, setLoaded);
    }, [])

    useEffect(() => {
        // console.log(message)
        setStudentDetails(message.studentDetails)
    }, [message])

    return (
        <>
            <CourseSettings existingInformation={existingInformation} />
            {existingInformation.initialized ? <>
                <Alert id="loadingAlert" className={loaded ? "d-none" : ""} variant="secondary">Loading...</Alert>
                <h5 className="pt-2">The current average grade is {message.grade}</h5>

                <div className="col-lg-6 col-sm-12 col-md-12">
                    <Table className="pt-3" striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>Student Name</th>
                                <th>Student Grade</th>
                                <th>Details</th>
                            </tr>

                        </thead>
                        <tbody>
                            {studentDetails?.map((student_entry) => (
                                <tr key={student_entry["id"]}>
                                    <td key={student_entry["name"]}>{student_entry["name"]}</td>
                                    <td key={student_entry["grade"]}>{student_entry["grade"]}</td>
                                    <td key={student_entry["id"]}>
                                        <StudentDetailModal student={student_entry} course_id={existingInformation.id} />
                                    </td>
                                </tr>
                            ))}

                        </tbody>
                    </Table>
                </div>
            </>
                : <></>}

        </>);
}