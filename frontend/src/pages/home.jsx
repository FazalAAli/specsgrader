import React, { useState } from "react";
import CustomNavbar from "../components/nav";
import CustomContent from "../components/content";
import CourseDashboard from "../components/courseDashboard"
import { useCookies } from "react-cookie";

export default function Home() {
    const [cookies, setCookie] = useCookies(['user']);
    const [loggedIn, setLoggedIn] = useState(cookies.uid ? true : false);

    return (
        <>
            <CustomNavbar loginState={loggedIn} loginSetter={setLoggedIn}></CustomNavbar>
            <CustomContent loginState={loggedIn}>
                <div className="container p-3 pt-5 offset-1">
                    <h3 className="pb-4">Dashboard</h3>

                    <div className="row">
                        <CourseDashboard />
                    </div>
                </div>
            </CustomContent>
        </>

    );
}
