import React, { useState } from "react";
import CustomNavbar from "../components/nav";
import CustomContent from "../components/content";
import { useCookies } from "react-cookie";
import { useLocation } from "react-router-dom";
import InstructorDetailsPage from "../pages/instructorDetailPage";
import StudentDetailsPage from "../pages/studentDetailPage";

export default function CourseDetails() {
    const [cookies] = useCookies(['user']);
    const [loggedIn, setLoggedIn] = useState(cookies.uid ? true : false);
    let data = useLocation();
    const courseName = data.state ? data.state.name : "Name";

    return (
        <>
            <CustomNavbar loginState={loggedIn} loginSetter={setLoggedIn}></CustomNavbar>
            <CustomContent loginState={loggedIn}>
                <div className="container p-3 pt-5 offset-1">
                    <h3 className="pb-4">{courseName}</h3>

                    {data.state.enrollment === "teacher" ?
                        <InstructorDetailsPage existingInformation={data.state} />
                        :
                        <StudentDetailsPage existingInformation={data.state} />
                    }

                </div>
            </CustomContent>
        </>

    );
}
