import './App.css';

import Home from './pages/home';
import CourseDetails from "./pages/courseDetails"
import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom";


function App() {
  return (
    <div className='background min-vh-100'>
      <Router>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/courseDetails/:id/" element={<CourseDetails />} />

        </Routes>
      </Router>
    </div >
  );
}

export default App;
