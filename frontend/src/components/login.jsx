import React from "react";
import OAuth2Login from 'react-simple-oauth2-login';
import logout from "../assets/imgs/logout.png"
import user from "../assets/imgs/user.png"
import "../assets/css/nav.css"
// import { useEffect } from "react";
import { useCookies } from 'react-cookie'


function LoginHandler(props) {
    const [cookies, setCookie] = useCookies(['user']);

    async function onSuccess(response) {
        var code = response["code"];
        var endpoint = process.env.REACT_APP_FLASK_API + "/login"
        var authenticate = await fetch(endpoint, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'credentials': 'include',
            },
            body: JSON.stringify({
                canvas_code: code
            })
        });

        var parsed = await authenticate.json()

        setCookie("uid", parsed["uid"], { path: "/" })

        props.loginSetter(true);
    };

    const onFailure = response => console.error(response);

    return <a>
        <img className='d-inline-block align-top userImg' src={user} />
        <OAuth2Login className="login navLinks"
            authorizationUrl={process.env.REACT_APP_CANVAS_URL + "/login/oauth2/auth"}
            responseType="code"
            clientId={process.env.REACT_APP_CANVAS_CLIENT_ID}
            redirectUri={process.env.REACT_APP_REDIRECT_URI}
            onSuccess={onSuccess}
            onFailure={onFailure}
        />
    </a>

}

export function LogoutHandler(props) {

    const [cookies, setCookie, removeCookie] = useCookies(["user"])

    async function logoutRemoveCookie(e) {
        e.preventDefault();


        await fetch(process.env.REACT_APP_FLASK_API + "/logout", {
            credentials: "include"
        })

        removeCookie("uid", { path: "/" });

        props.loginSetter(false);
    }

    return (
        <a href="" className="logoutLink" onClick={logoutRemoveCookie}>
            <img className='d-inline-block align-top userImg' src={logout} />
            <span className="navLinks">Logout</span>
        </a>
    );
}

export default LoginHandler;