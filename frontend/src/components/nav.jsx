import React from 'react'
import { Container, Navbar, Nav } from 'react-bootstrap'
import utlogo from "../assets/imgs/ut.png"
import glasses from "../assets/imgs/glasses.png"
import user from "../assets/imgs/user.png"
import LoginHandler, { LogoutHandler } from './login'

import "../assets/css/nav.css"

function CustomNavbar(props) {


    return (
        <Navbar className="customNav" expand="lg">
            <Container>
                <Navbar.Brand href="https://utexas.edu">
                    <img
                        src={utlogo}
                        className="d-inline-block align-top utLogo"
                        alt="Utexas homepage"
                    />
                </Navbar.Brand>

                <Navbar.Brand href="/" className='offset-3'>
                    <img
                        src={glasses}
                        className="d-inline-block align-top glasses"
                        alt="Homepage"
                    />
                    <h2 className='d-inline-block mainHeading'>SpecsGrader</h2>
                </Navbar.Brand>

                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav" className='offset-4 col-3'>
                    <Nav className="me-auto">
                        {props.loginState ?
                            <>
                                <Nav.Link href="/"><img className='d-inline-block align-top userImg' src={user} />
                                    <span className='navLinks'>My Courses</span>
                                </Nav.Link>

                                <LogoutHandler {...props} />
                            </>
                            :
                            <LoginHandler {...props} />
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )

}

export default CustomNavbar;