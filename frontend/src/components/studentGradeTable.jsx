import { useEffect, useState } from "react"
import { Table } from "react-bootstrap"
import { getStudentAssignments } from "../components/utility"

export default function StudentGradeTable(props) {
    const [message, setMessage] = useState([])
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        setLoaded(false)
        getStudentAssignments(props.student_id, props.course_id, setMessage, setLoaded);
    }, [])

    return (
        <>
            <Table className="pt-3" striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th>Assignment Name</th>
                        <th>Grade</th>
                    </tr>

                </thead>
                <tbody>
                    {message?.map((assignment_entry) => (
                        <tr key={assignment_entry["id"]}>
                            <td key={assignment_entry["name"]}>{assignment_entry["name"]}</td>
                            <td key={assignment_entry["score"]}>{assignment_entry["score"] || "None"} </td>
                        </tr>
                    ))}

                </tbody>
            </Table>
        </>);
}