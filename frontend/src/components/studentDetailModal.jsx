import { Modal, Button } from "react-bootstrap"
import { React, useState } from "react";
import StudentGradeTable from "../components/studentGradeTable"

export default function StudentDetailModal(props) {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <Button variant="primary" onClick={handleShow}>
                Details
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{props.student["name"]} : {props.student["grade"]}</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <StudentGradeTable student_id={props.student["id"]} course_id={props.course_id} />

                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}