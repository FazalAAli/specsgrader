import React from "react";
import { Table, Button, Alert } from 'react-bootstrap'
import { useState } from "react";
import { useEffect } from "react";
import { Link } from "react-router-dom";
import { getAllCourseData } from "../components/utility"

export default function CourseDashboard() {
    // const [loaded, setLoaded] = useState(false);
    const [message, setMessage] = useState([])
    const [refresher, setRefresher] = useState(0)
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        setLoaded(false)
        getAllCourseData(refresher !== 0, setMessage, setLoaded);
    }, [refresher]);

    return <div className="col-lg-6 col-sm-12 col-md-12">
        <Alert id="loadingAlert" className={loaded ? "d-none" : ""} variant="secondary">Loading...</Alert>
        <Table striped bordered hover variant="dark">
            <thead>
                <tr>
                    <th>Course code</th>
                    <th>Course Name</th>
                    <th>Grade</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {message.map((course_entry) => (
                    <tr key={course_entry["id"]}>
                        <td key={course_entry["course_code"]}>{course_entry["course_code"]}</td>
                        <td key={course_entry["name"]}>{course_entry["name"]}</td>
                        <td key={course_entry["grade"]}>{course_entry["grade"]}</td>
                        <td key={course_entry["id"]}>
                            <Link style={{ color: "white" }} to={"/courseDetails/" + course_entry["id"]} state={course_entry}>
                                View details
                            </Link>
                        </td>
                    </tr>
                ))}
            </tbody>
        </Table>
        <center>
            <Button onClick={(e) => setRefresher(refresher + 1)} className="pt-2 primary" >Refresh grades</Button>
        </center>
    </div>

}