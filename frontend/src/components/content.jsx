import React from "react";
import { Card } from "react-bootstrap";

export default function CustomContent(props) {
    return (
        <>
            {props.loginState ?
                <>
                    {props.children}
                </>
                :
                <div className="container mt-5 pt-5">
                    <Card className="col-12 justify-content-center p-5 align-items-center">
                        <h3>Please use the Login Button above to login with Canvas</h3>
                        <h5>
                            For any queries or issues, please create a gitlab issue &nbsp;
                            <a className="link-primary" href="https://gitlab.com/FazalAAli/specsgrader">here</a>
                        </h5>
                    </Card>

                </div>
            }
        </>
    );
}