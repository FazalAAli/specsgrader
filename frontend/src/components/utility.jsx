export async function getAllCourseData(update, setMessage, setLoaded) {
    var test = await fetch(process.env.REACT_APP_FLASK_API + "/courses", {
        method: "POST",
        credentials: "include",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "update": update
        })
    })
    var info = await test.json();
    setMessage(info)
    setLoaded(true)
}

export async function getSingleCourseDataInstructor(id, setMessage, setLoaded) {
    var test = await fetch(process.env.REACT_APP_FLASK_API + "/courses/instructor", {
        method: "POST",
        credentials: "include",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "course_id": id
        })
    })

    var info = await test.json();
    setMessage(info)
    setLoaded(true)
}

export async function getStudentAssignments(student_id, course_id, setMessage, setLoaded) {

    var test = await fetch(process.env.REACT_APP_FLASK_API + `/courses/student/assignments`, {
        method: "POST",
        credentials: "include",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "student_id": student_id,
            "course_id": course_id
        })
    })

    var info = await test.json();
    setMessage(info)
    setLoaded(true)
}