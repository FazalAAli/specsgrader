# Update and Base dependencies
sudo apt update
sudo apt upgrade
sudo apt install -y build-essential
sudo apt install -y libssl-dev libreadline-dev zlib1g-dev

# rbenv and rbenv build install

git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
source ~/.bashrc

#rbenv insall command fix
git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build

rbenv install 2.6.6

rbenv global 2.6.6

gem install dory

#docker installation

sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-cache policy docker.io
sudo apt -y install docker.io

#docker user gruop

newgrp docker
sudo usermod -aG docker ${USER}
sudo apt install -y docker-compose

# dory name resolution fix
sudo apt install -y resolvconf
sudo echo "nameserver 8.8.8.8" >> /etc/resolvconf/resolv.conf.d/head
sudo echo "nameserver 8.8.4.4" >> /etc/resolvconf/resolv.conf.d/head

git clone https://github.com/instructure/canvas-lms.git
cd canvas-lms
git checkout stable/2022-05-11

#file permission fix
cd ../
chown -R 1000:1000 canvas-lms
cd canvas-lms
sudo setfacl -Rm u:9999:rwX,g:9999:rwX .
sudo setfacl -dRm u:9999:rwX,g:9999:rwX .
./script/docker_dev_setup.sh

# If no init prompt - docker-compose run --rm web bundle exec rake db:initial_setup

# For answers to the docker script prompts, please select no for mutagen-compose and yes for everything else